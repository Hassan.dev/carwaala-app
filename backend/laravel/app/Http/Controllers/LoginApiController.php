<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class LoginApiController extends Controller
{
    //login api
    public function postLogin()
    {
        $userEmail = request()->input('email');
        $userPassword = request()->input('password');

        if(User::where('email', $userEmail)->where('password', $userPassword)->first()){
            $user = User::where('email', $userEmail)->first();
            return [
                'status' => 1,
                'message' => $user->name
            ];
        }else{
            return [
              'status' => 0,
                'message' => 'Wrong credentials, try again'
            ];
        }
    }

    //register api
    public function postRegister()
    {
        $validate = Validator::make(request()->all(), [
           'name' => 'required|min:6',
           'email' => 'required|unique:users',
           'password' => 'required|min:6'
        ]);

        if($validate->fails()){
            return [
              'status' => 0,
              'message' => $validate->errors()->first()
            ];
        }else{
            $name = request()->input('name');
            $email = request()->input('email');
            $password = request()->input('password');

            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->password = $password;

            $user->save();

            return [
              'status' => 1,
              'message' => 'You successfully registered, please login.'
            ];
        }
    }
}
