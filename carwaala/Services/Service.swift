//
//  Service.swift
//  carwaala
//
//  Created by Admin on 08/05/1440 AH.
//  Copyright © 1440 Carwaala. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class Service{
    let baseURL = "http://127.0.0.1:8000/"
    
    init(){
    }
    
    //login api
    
    func login(parameters: [String: AnyObject], completion: @escaping (_ callback: JSON)->()) {
        let url = baseURL + "api/login"
        Alamofire.request(url , method: .post, parameters: parameters).responseJSON { response in
            if let json = response.result.value {
                completion(JSON(json))
            } else{
                print("not valid json")
            }
        }
    }
    
    
    
    
}
