//
//  LoginVC.swift
//  carwaala
//
//  Created by Admin on 08/05/1440 AH.
//  Copyright © 1440 Carwaala. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginVC: UIViewController {
    
    @IBOutlet weak var WrongCredentials: UILabel!
    @IBOutlet weak var PasswordError: UILabel!
    @IBOutlet weak var EmailError: UILabel!
    @IBOutlet weak var SuccessMessage: UILabel!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginButton(_ sender: Any) {
        
        if txtEmail.text == "" {
            self.EmailError.text = "Please enter email"
        }
        if txtPassword.text == "" {
            self.PasswordError.text = "Please enter password"
        }
        
        let paramters: [String: AnyObject] = ["email": txtEmail.text! as AnyObject, "password": txtPassword.text! as AnyObject]
        
        Service().login(parameters: paramters) { (response) in
            
            if response["status"].intValue == 0 {
                print("login unsuccessfull")
                self.WrongCredentials.text = "Wrong credentials"
                
            } else {
                let userName = response["message"]
                print("UserID \(String(describing: userName))")
                
                self.SuccessMessage.text = "Login Successfull \(userName)"
                self.EmailError.text = ""
                self.PasswordError.text = ""
                self.WrongCredentials.text = ""
                
                self.performSegue(withIdentifier: "dashboard", sender: self)
            }
        }
    }
    

}
