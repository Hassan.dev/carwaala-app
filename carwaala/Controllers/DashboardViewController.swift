//
//  DashboardViewController.swift
//  carwaala
//
//  Created by Admin on 09/05/1440 AH.
//  Copyright © 1440 Carwaala. All rights reserved.
//

import UIKit
import IconFontKit

class DashboardViewController: UIViewController {
    @IBOutlet weak var lbl_header: UILabel!
    @IBAction func btn_toggler(_ sender: UIButton) {
    }
    @IBOutlet weak var lbl_app_name: UILabel!
    @IBOutlet weak var lbl_page_name: UILabel!
    @IBOutlet var lbl_search: [UISearchBar]!
    
    @IBOutlet var lbl_body: [UILabel]!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
